package primes;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.lang.Math;

public class SimplePrimesLister {
	static ArrayList<Integer> primes = new ArrayList<Integer>();
	/**
	 * Return a sorted list -- sorted in ascending order -- of all integers that
	 * are prime and are less than or equal to n.
	 * 
	 * @param n
	 * @return a list of integers that contains the prime numbers <= n. If n <= 1 then the list that is returned is empty.
	 */
	public static ArrayList<Integer> getPrimesLessThanEqualTo(Integer n) {
		
		for(int i=2;i<=n;i++){
			int val=SimplePrimesLister.isPrime(i);
			if(val!=0) primes.add(val);
			
		}
		
		
		
		
		return primes;
	
		
		// TODO: Implement this method
			
	}
	
	public static int isPrime(int p){
		
		int factors = 0;
		int testupto = (int) Math.ceil(Math.sqrt(p));
		
		for(int i=1;i<=testupto;i++){
			int wholenum=p%i;
			if(wholenum==0){
				factors++;
			}
			
			
		}
		if(p==2) return p;
		if(factors>1) return 0;
		else return p;
	}
	public static void main (String args []){
		int t=2;
		int r=SimplePrimesLister.isPrime(t);
		
		System.out.println(SimplePrimesLister.getPrimesLessThanEqualTo(t));
	}

}
