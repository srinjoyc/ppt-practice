package primes;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class SimplePrimesListerTest {

	@Test
	public void test1() {
		Integer[] primes = {};
		List<Integer> list1 = Arrays.asList(primes); 
		List<Integer> list2 = SimplePrimesLister.getPrimesLessThanEqualTo(1);
		assertEquals(list1, list2);
	}

	@Test
	public void test2() {
		Integer[] primes = {2};
		List<Integer> list1 = Arrays.asList(primes);
		List<Integer> list2 = SimplePrimesLister.getPrimesLessThanEqualTo(2);
		assertEquals(list1, list2);
	}

	@Test
	public void test5() {
		Integer[] primes = {2, 3, 5};
		List<Integer> list1 = Arrays.asList(primes);
		List<Integer> list2 = SimplePrimesLister.getPrimesLessThanEqualTo(5);
		assertEquals(list1, list2);
	}
	
	@Test
	public void test20() {
		Integer[] primes = {2, 3, 5, 7, 11, 13, 17, 19};
		List<Integer> list1 = Arrays.asList(primes);
		List<Integer> list2 = SimplePrimesLister.getPrimesLessThanEqualTo(20);
		assertEquals(list1, list2);
	}

	@Test
	public void test37() {
		Integer[] primes = {2, 3, 5, 7, 11, 13, 17, 19};
		List<Integer> list1 = Arrays.asList(primes);
		List<Integer> list2 = SimplePrimesLister.getPrimesLessThanEqualTo(37);
		assertFalse(list1.equals(list2));		
	}
	
	@Test
	public void testNegative() {
		Integer[] primes = {};
		List<Integer> list1 = Arrays.asList(primes);
		List<Integer> list2 = SimplePrimesLister.getPrimesLessThanEqualTo(-15);
		assertEquals(list1, list2);
	}
	
}
