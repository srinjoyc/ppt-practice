package variablenames;

public class SnakeCaseName extends VariableName {

	/**
	 * Create a SnakeCaseName object with a given String. Like all
	 * VariableNames, SnakeCaseNames cannot contain digits and nor can the names
	 * start with the '_' character.
	 * 
	 * @param varName
	 *            the String that is used to create the SnakeCaseName
	 */
	public SnakeCaseName(String varName) {
		// TODO: Implement this
		super("blah");
	}

	@Override
	protected String format(String varName) {
		// TODO: Implement this?
		return "blah";
	}

	/**
	 * Convert to SnakeCase format
	 * 
	 * @return a CamelCaseName for this VariableName that is in SnakeCase
	 */
	public CamelCaseName toCamelCase() {
		// TODO: Implement this method
		return new CamelCaseName("test"); // this should change!
	}
}
