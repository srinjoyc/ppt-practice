package variablenames;

import static org.junit.Assert.*;

import org.junit.Test;

public class VariableNameTest {

	@Test
	public void testCamelCase1() {
		VariableName varName = new CamelCaseName("   1 X2 2y z ABC");
		String shouldBe = "xYZAbc";
		assertEquals(varName.toString(), shouldBe);
	}
	
	@Test
	public void testSnakeCase1() {
		VariableName varName = new SnakeCaseName("   1 X2 2y z ABC");
		String shouldBe = "x_y_z_abc";
		assertEquals(varName.toString(), shouldBe);
	}
	
	@Test
	public void testCamelCase2() {
		VariableName varName = new CamelCaseName("-1X yz");
		String shouldBe = "xYz";
		assertEquals(varName.toString(), shouldBe);
	}
	
	@Test
	public void testSnakeCase2() {
		VariableName varName = new SnakeCaseName("-1X yz");
		String shouldBe = "x_yz";
		assertEquals(varName.toString(), shouldBe);	
	}

	@Test
	public void testCamelCase3() {
		VariableName varName = new CamelCaseName("x_y_z");
		String shouldBe = "xYZ";
		assertEquals(varName.toString(), shouldBe);	
	}	
	
	@Test
	public void testSnakeCase3() {
		VariableName varName = new SnakeCaseName("x_y_z");
		String shouldBe = "x_y_z";
		assertEquals(varName.toString(), shouldBe);	
	}	
	
	@Test
	public void testCamelCase4() {
		VariableName varName = new CamelCaseName("Hello World");
		String shouldBe = "helloWorld";
		assertEquals(varName.toString(), shouldBe);	
	}	
	
	@Test
	public void testSnakeCase4() {
		VariableName varName = new SnakeCaseName("Hello World");
		String shouldBe = "hello_world";
		assertEquals(varName.toString(), shouldBe);	
	}	
	
	@Test
	public void testCamelCase5() {
		VariableName varName = (new CamelCaseName("Hello World")).toSnakeCase();
		String shouldBe = "hello_world";
		assertEquals(varName.toString(), shouldBe);	
	}	
	
	@Test
	public void testSnakeCase5() {
		VariableName varName = (new SnakeCaseName("Hello World")).toCamelCase();
		String shouldBe = "helloWorld";
		assertEquals(varName.toString(), shouldBe);	
	}	

}
