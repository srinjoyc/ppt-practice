package variablenames;

public class CamelCaseName extends VariableName {
	/**
	 * Create a CamelCaseName object with a given String. Like all
	 * VariableNames, CamelCaseNames cannot contain digits and nor can the names
	 * start with the '_' character.
	 * 
	 * @param varName
	 *            the String that is used to create the CamelCaseName
	 */
	public CamelCaseName(String varName) {
		// TODO: Implement this method correctly
		super("blah");
	}

	@Override
	protected String format(String varName) {
		// TODO: Should you do something here?
		return "blah";
	}

	/**
	 * Convert a VariableName in CamelCase format to SnakeCase format.
	 * 
	 * @return a SnakeCaseName for this VariableName this is in CamelCase
	 */
	public SnakeCaseName toSnakeCase() {
		// TODO: Implement this method
		return new SnakeCaseName("blah");
	}
}