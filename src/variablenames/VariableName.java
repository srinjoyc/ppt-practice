package variablenames;

public abstract class VariableName {
	private final String varName; // the String that holds the variable name in
									// the appropriate format

	/**
	 * This constructor will create a Variable Name if the provided argument is
	 * a valid variable name (i.e., it does not contain whitespace) else it will
	 * strip whitespace and create a variable name that has all the remaining
	 * characters in the same order. Further, a variable name may not contain
	 * digits so digits should be eliminated.
	 * 
	 * @param varName
	 * @requires varName != null and varName is not an empty String
	 * @throws IllegalArgumentException
	 *             if varName != null and varName is not an empty String
	 * 
	 */
	public VariableName(String varName) throws IllegalArgumentException {

		if (varName == null || varName.equals("")) {
			throw new IllegalArgumentException("Invalid String object");
		}

		this.varName = format(varName);

	}

	/**
	 * 
	 * @param varName
	 * @return a String that is formatted correctly as a VariableName
	 * @requires varName != null and varName != ""
	 */
	protected String format(String varName) {
		// Using regex here. Could do this in a loop too!
		return varName.trim().replaceAll("\\s+", "").replaceAll("\\d+", "")
				.replaceAll("\\W+", "");
	}

	/**
	 * Return a String representation of the variable name
	 * 
	 * @return a String that represents the variable name
	 */
	public String toString() {
		// simple enough!
		return varName;
	}
}
